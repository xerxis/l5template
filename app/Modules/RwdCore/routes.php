<?php

// All Views Shares ( Relative to root folder )
View::share('ModAssets', [
		'css' => asset('assets/css/'),	
		'js' => asset('assets/js/'),
		'img' => asset('assets/img/'),		
		'lib' => asset('assets/lib/'),
		'uploads' => asset('uploads/')		
		]);


Route::group(array('module' => 'RwdCore', 'namespace' => 'App\Modules\RwdCore\Controllers'), function() {

	//API Call Version 1
	Route::group(array('prefix' => 'api'), function(){

		/*
		 * Add New Registrant
		 * Usage : http://domain.com/api/new-registrant
		 * Params : param1=xerxis&param2=alvar
		 */
		//Route::post('/new-registrant', 'OakleySEAGRegAPIController@AddNewRegistrants' );

	
	});	


    	// SITE Pages
	Route::group(array('prefix' => '/'), function(){

		// Registration
		Route::get('/', ['as' => 'home', 'uses' => 'RwdCoreController@site_home' ]);


	});	
    
});	
