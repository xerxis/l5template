      {{-- Minified  JS Libraries --}}
      {!! 
      	Minify::javascript(array(
          
          $ModAssets['lib'].'/jquery/jQuery-2.1.3.min.js',
          $ModAssets['lib'].'/bootstrap/js/bootstrap.min.js',
          $ModAssets['js'].'/defaults.js',


      	))->withFullUrl(); 
      !!}

