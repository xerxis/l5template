<?php namespace App\Modules\RwdCore\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class RwdCoreController extends Controller {


	public function site_home() {
		return view( 'RwdCore::pages.home' );
	}


}
